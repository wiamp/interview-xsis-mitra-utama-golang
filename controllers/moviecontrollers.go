package controllers

import (
	"encoding/json"
	"interview-xsis-mitra-utama-golang/database"
	"interview-xsis-mitra-utama-golang/entity"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// GetAllMovie get all movie data
func GetAllMovie(w http.ResponseWriter, r *http.Request) {
	var movies []entity.Movie

	database.Connector.Where("deleted_at IS NULL").Find(&movies)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(movies)
}

// GetMovieByID returns movie with specific ID
func GetMovieByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	var movie entity.Movie
	database.Connector.Where("deleted_at IS NULL").First(&movie, key)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(movie)
}

// CreateMovie creates movie
func CreateMovie(w http.ResponseWriter, r *http.Request) {
	requestBody, _ := ioutil.ReadAll(r.Body)
	var movie entity.MovieData
	json.Unmarshal(requestBody, &movie)

	database.Connector.Create(movie)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(movie)
}

// UpdateMovieByID updates movie with respective ID
func UpdateMovieByID(w http.ResponseWriter, r *http.Request) {
	requestBody, _ := ioutil.ReadAll(r.Body)
	var movie entity.MovieData
	json.Unmarshal(requestBody, &movie)
	database.Connector.Save(&movie)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(movie)
}

// DeleteMovieByID delete's movie with specific ID
func DeleteMovieByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	var movie entity.Movie
	id, _ := strconv.ParseInt(key, 10, 64)
	database.Connector.Where("id = ?", id).Delete(&movie)
	w.WriteHeader(http.StatusNoContent)
}
