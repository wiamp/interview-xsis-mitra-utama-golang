## Installation

- Create database and set database configuration in config.js
- Import database.sql into database
- Config your database configuration in main.go
- Make sure you already install nodejs in your machine
- Copy files into your development folder and open terminal in root folder
- Run the code by typing `go run main.go`

## NOTE