package entity

//Movie object for REST(CRUD)
type Movie struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Rating      string `json:"rating"`
	Image       string `json:"image"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

func (Movie) TableName() string {
	return "movie"
}

type MovieData struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Rating      string `json:"rating"`
	Image       string `json:"image"`
}

func (MovieData) TableName() string {
	return "movie"
}
