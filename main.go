package main

import (
	"interview-xsis-mitra-utama-golang/controllers"
	"interview-xsis-mitra-utama-golang/database"
	"interview-xsis-mitra-utama-golang/entity"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql" //Required for MySQL dialect
)

func main() {
	initDB()
	log.Println("Starting the HTTP server on port 3000")

	router := mux.NewRouter().StrictSlash(true)
	initaliseHandlers(router)
	log.Fatal(http.ListenAndServe(":3000", router))
}

func initaliseHandlers(router *mux.Router) {
	router.HandleFunc("/movie", controllers.GetAllMovie).Methods("GET")
	router.HandleFunc("/movie/{id}", controllers.GetMovieByID).Methods("GET")
	router.HandleFunc("/movie", controllers.CreateMovie).Methods("POST")
	router.HandleFunc("/movie/{id}", controllers.UpdateMovieByID).Methods("PUT")
	router.HandleFunc("/movie/{id}", controllers.DeleteMovieByID).Methods("DELETE")
}

func initDB() {
	config :=
		database.Config{
			ServerName: "127.0.0.1:3306",
			User:       "root",
			Password:   "",
			DB:         "xsis_mitra_utama",
		}

	connectionString := database.GetConnectionString(config)
	err := database.Connect(connectionString)
	if err != nil {
		panic(err.Error())
	}
	database.Migrate(&entity.Movie{})
}
